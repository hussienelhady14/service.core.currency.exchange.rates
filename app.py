from flask_cors import CORS
from os import environ
import connexion
from connexion.resolver import RestyResolver
import os 

def decode_token(token):
    return {"token":token}

app = connexion.FlaskApp(__name__)
app.add_api('./handlers/swagger/exchangecontroller.yml', resolver=RestyResolver('handlers'))
flask_app = app.app
CORS(flask_app)

if __name__ == '__main__':

    if str(environ.get('VCAP_APPLICATION')) == 'None':

            HOST = environ.get('SERVER_HOST', '0.0.0.0') 
            try:
                PORT = int(environ.get('SERVER_PORT', '5000'))
            except ValueError:
                PORT = 5000
        
            flask_app.run(host=HOST, port=PORT)

    else:
        
            port = os.getenv('VCAP_APP_PORT', '8080')
            flask_app.run(host='0.0.0.0', port=int(port))
