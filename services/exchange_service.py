import requests
from helpers.properties import properties
from helpers.headers.headers import headers
from helpers.yaml.yamlmanager import yamlmanager
from helpers.sql.mysql_connector import mysql_connector
from helpers.sql.sqlqueryexecutor import sqlqueryexecutor
from services.interfaces.service_interface import service_interface





class exchange(service_interface):
    
    def __init__(self, date, base):
        self._date = date
        self._base = base
        self._response = None
        self._config = None
        self._env = None
        self._db_conn_attributes = None

    
    @property
    def date(self):
        return self._date

    @date.setter
    def date(self, date):
        self._date = date

    @property
    def base(self):
        return self._base

    @base.setter
    def base(self, base):
        self._base = base

    def fire(self):
        """ function to get exchange rates through api-layer
                Args:
                    private args 
                    
                Returns:    

                Status, http req code, message
        """

        #appconfig object
        yamlobj = yamlmanager(properties._APP_CONFIG_PATH, properties._APP_ENV_PATH, properties._ENV)
        configstatus, config, env = yamlobj.config_getter()
        self._config = config
        self._env = env

        #constructing request url
        requesturl = config['env'][env]['base']['api-layer'] + config['path']['get-exchange-rate-by-date'].format(str(self._date), 
        str(self._base))

        #construct request headers
        requestheaders = headers(config['env']['token']).get_headers()
        
        #firing exchange request
        request = requests.get(requesturl, headers=requestheaders, verify=False)

        #assesing request response
        if(request.status_code == properties._SUCCESS_CD):
            self._response = request.json()

            #Inserting in database and assessing response
            if not self.db_ingest():
                return False, properties._CONFLICT_CD, properties._INGESTION_FAILURE

            return True, request.status_code, request.json()
        else:
            try: 
                message = request.json()
            except Exception:
                message = properties._EXCHANGE_FAILURE

            return  False, request.status_code, message

    #Database ingestion function
    def db_ingest(self):
        query = properties._EXCHANGE_INGESTION_QUERY
        sqlexecobj = sqlqueryexecutor(query, properties._QUERY_OPERATOR, mysql_connector, self.db_connection_attributes())
        filtered_rates = dict(filter(lambda elem: elem[0] in ('USD', 'EUR'), self._response['rates'].items()))
        for key, value in filtered_rates.items():
            sqlexecobj._query = properties._EXCHANGE_INGESTION_QUERY.format(str(key), str(value), str(self._date), str(value))
            status, message = sqlexecobj.query_execute()
            if not status:
                return status
        
        return True

    #Returning database connection attributes
    def db_connection_attributes(self):
        return {
            "host": self._config['db']['mysql']['host'],
            "username": self._config['db']['mysql']['username'],
            "password": self._config['db']['mysql']['password'],
            "dbname": self._config['db']['mysql']['dbname'],
        }