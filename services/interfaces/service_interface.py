from abc import ABC, abstractmethod

class service_interface(ABC):
    @abstractmethod
    def fire(self):
        pass