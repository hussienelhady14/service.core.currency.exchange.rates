#Create table replace {SCHEMA}
CREATE TABLE {SCHEMA}.exchange_rates
( id INT(11) NOT NULL AUTO_INCREMENT,
  currency_name VARCHAR(3) NOT NULL,
  currency_value DECIMAL(10,6),
  exchange_rate_dt DATE,
  ingest_dts DATETIME DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT exchange_pk PRIMARY KEY (id, currency_name, exchange_rate_dt)
);

#Add unique contsraint replace {SCHEMA}
ALTER TABLE {SCHEMA}.exchange_rates
ADD CONSTRAINT unique_row UNIQUE (currency_name, exchange_rate_dt);

#Partition table as per requirements replace {SCHEMA}
ALTER TABLE {SCHEMA}.exchange_rates
PARTITION BY RANGE(MONTH(exchange_rate_dt) )
SUBPARTITION BY HASH( DAY(exchange_rate_dt) )
SUBPARTITIONS 31 (
    PARTITION p0 VALUES LESS THAN (2),
    PARTITION p1 VALUES LESS THAN (3),
    PARTITION p2 VALUES LESS THAN (4),
    PARTITION p3 VALUES LESS THAN (5),
    PARTITION p4 VALUES LESS THAN (6),
    PARTITION p5 VALUES LESS THAN (7),
    PARTITION p6 VALUES LESS THAN (8),
    PARTITION p7 VALUES LESS THAN (9),
    PARTITION p8 VALUES LESS THAN (10),
    PARTITION p9 VALUES LESS THAN (11),
    PARTITION p10 VALUES LESS THAN (12),
    PARTITION p11 VALUES LESS THAN MAXVALUE
);