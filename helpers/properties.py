import json
from flask import Response, request
from http.client import UNAUTHORIZED

#Class holding all constant values
class properties():

    _CREATED_CD = 201

    _SUCCESS_CD = 200

    _CONFLICT_CD = 409

    _NOT_FOUND_CD = 404

    _NO_CONTENT_CD = 204

    _BAD_REQUEST_CD = 400

    _UNAUTHORIZED_CD = 401

    _SERVICE_UNAVAILABLE_CD = 503

    _ENV = "ENV"

    _AUTHORIZATION_HEADER = "apikey"
    
    _MIME_TYPE = 'application/json'

    _APP_ENV_PATH = "./config/.env"

    _QUERY_OPERATOR = "STRING_QUERY"

    _APP_CONFIG_PATH = "./config/appconfig.yaml"

    _EXCHANGE_FAILURE = {"message": "failure in getting exchange rates"}

    _INGESTION_FAILURE = {"message": "failure in ingesting exchange rates"}

    _EXCHANGE_INGESTION_QUERY = "INSERT INTO SCHEMA.exchange_rates (currency_name, currency_value, exchange_rate_dt) VALUES ('{}', '{}', '{}') ON DUPLICATE KEY UPDATE currency_value='{}'"