import os
import pandas as pd
from dotenv import load_dotenv
from helpers.yaml.interface_yaml import interface_yaml


class env(interface_yaml):

    def __init__(self, envpath,envkey):

        load_dotenv(envpath)
        self._env = os.getenv(envkey) 
    

    @property
    def env(self):
        return self._env
    @env.setter
    def env(self, env):
        self._env = env

    def file_serializer(self):
        return  self._env 
