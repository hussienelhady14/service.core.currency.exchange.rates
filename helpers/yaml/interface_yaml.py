from abc import ABC, abstractmethod

class interface_yaml(ABC):

    @abstractmethod
    def file_serializer(self):
        pass
