from datetime import datetime
from os import error
from helpers.yaml.yamlparse import yamlparse
from helpers.yaml.env import env


class yamlmanager():

    def __init__(self, yamlpath,envpath,envkey):
        self._env_object = env(envpath,envkey)
        self._yamlpath = yamlpath
       
        
    @property
    def env_object(self):
        return self._env_object
    @env_object.setter
    def env_object(self, env_object):
        self._env_object = env_object

    @property
    def yamlpath(self):
        return self.yamlpath
    @yamlpath.setter
    def yamlpath(self, yamlpath):
        self._yamlpath = yamlpath


    @property
    def name(self):
        return self._name
    @name.setter
    def name(self, name):
        self._name = name

    def config_getter(self):
        try:
            env  = self._env_object.file_serializer()
            yamlobj = yamlparse( self._yamlpath,env)
            return True, yamlobj.file_serializer(), env
        except error as e:
            return False, e, 'Yaml Parser Error'
        
