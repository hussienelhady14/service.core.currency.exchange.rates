import pandas as pd
import yaml
from helpers.yaml.interface_yaml import interface_yaml


class yamlparse(interface_yaml):

    def __init__(self,yamlpath,envobject):

        self._envobject = envobject
        self._yamlpath = yamlpath


    @property
    def envobject(self):
        return self._envobject
    @envobject.setter
    def envobject(self, envobject):
        self._envobject = envobject

    @property
    def yamlpath(self):
        return self._yamlpath
    @yamlpath.setter
    def yamlpath(self, yamlpath):
        self._yamlpath = yamlpath

    def file_serializer(self):
        
        with open(self._yamlpath) as f:

            config = yaml.load(f, Loader=yaml.FullLoader)
            self._envobject

        return config
