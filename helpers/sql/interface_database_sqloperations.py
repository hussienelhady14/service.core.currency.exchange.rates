import abc
from abc import ABC, abstractmethod

class interface_database_sqloperations(ABC):

    @abc.abstractmethod
    def read_execute(self):
        pass

    @abc.abstractmethod
    def query_execute(self):
        pass

    @abc.abstractmethod
    def query_operator_route(self):
        pass

