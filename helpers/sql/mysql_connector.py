import pyodbc
from os import error
from helpers.sql.interface_database_connector import interface_database_connector


class mysql_connector(interface_database_connector):
    """Establish a connection to mysql DB.
    
    Attributes:
        connection_attributes: a dictionary containing the connection attributes.
                {
                'host' (str) (required), 
                'username' (str) (required),
                'password' (str) (required),
                'dbname' (str) (required)
                }
    """

    def __init__(self, connection_attributes:dict):
        """Inits mysql_connector with connection_attributes."""
        self._host = connection_attributes.get('host')
        self._username = connection_attributes.get('username')
        self._password = connection_attributes.get('password')
        self._driver = "MySQL ODBC 8.0 Unicode Driver"
        self._dbname = connection_attributes.get('dbname')
        self._conn = None

    @property
    def host(self):
        return self._host
    @host.setter
    def host(self, host):
        self._host = host
    @property
    def username(self):
        return self._username
    @username.setter
    def username(self, username):
        self._username = username
    @property
    def password(self):
        return self._password
    @password.setter
    def password(self, password):
        self._password = password
    @property
    def dbname(self):
        return self._dbname
    @dbname.setter
    def dbname(self, dbname):
        self._dbname = dbname

    def connect(self):
        """Connects to mysql DB."""
        try:
            sqlserverstr = f"DRIVER={self._driver};Server={self._host};DATABASE={self._dbname};UID={self._username};PWD={self._password};"
            self._conn = pyodbc.connect(sqlserverstr)
            return True, "MySQL connected successfully"
        except pyodbc.Error as ex:
            return False, str(ex)

    def disconnect(self):
        """Disconnects from mysql DB."""
        try:
            csr = self._conn.cursor()  
            csr.close()
            del csr
            return True,"MySQL disconnected successfully"
        except error as ex:
            return False,str(ex)
