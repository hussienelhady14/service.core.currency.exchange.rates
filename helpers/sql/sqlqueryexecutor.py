import time
import pandas as pd
from helpers.sql.interface_database_connector import interface_database_connector
from helpers.sql.interface_database_sqloperations import interface_database_sqloperations


class sqlqueryexecutor(interface_database_sqloperations):
    """Executes queries from the specified connector.
    
    Attributes:
        file: a file containing the SQL query.
        database_connector: the connector used to execute the query.
        connection_attributes: a dictionary containing connection attributes,
                                                    should match the connector type.
        query_type = FILE_QUERY OR STRING_QUERY

    """
    def __init__(self,query,query_operator, database_connector:interface_database_connector, connection_attributes:dict):
        """Inits queryexecutor with file_path, database_connector and connection_attributes."""
        self._query = query
        self._database_connector = database_connector(connection_attributes)
        self._query_operator = query_operator

    @property
    def query(self):
        return self._query
    @query.setter
    def query(self, query):
        self._query = query

    @property
    def database_connector(self):
        return self._database_connector
    @database_connector.setter
    def database_connector(self, database_connector):
        self._database_connector = database_connector

    @property
    def query_operator(self):
        return self._query_operator
    @query_operator.setter
    def query_operator(self, query_operator):
        self._query_operator = query_operator


    def read_execute(self):
        """Executes the read query """

        operation_route = self.query_operator_route()

        if(operation_route[0]):

            status = self._database_connector.connect()
            if status[0]:
                try:
                    result = pd.read_sql_query(self._query, self._database_connector._conn)
                    self._database_connector.disconnect()
                    return True, result
                except TypeError as e:
                    return False, str(e)
            return status[0], status[1]

        return operation_route[0],operation_route[1]

    def query_execute(self):

        """exceutes cud queries"""
        retry_flag = True
        retry_count = 0

        operation_route = self.query_operator_route()

        if(operation_route[0]):

            while retry_flag and retry_count < 1000:
                try: 
                    status = self._database_connector.connect()
                    if status[0]:
                        cursor = self._database_connector._conn.cursor()
                        cursor.execute(self._query)
                        self._database_connector._conn.commit()
                        self._database_connector.disconnect()
                        retry_flag = False
                    return status[0], status[1]
                except Exception:
                    retry_count = retry_count + 1
                    time.sleep(1)

            if not retry_flag:
                return True,'Success Query Execution'    
            return False,'Failure Query Execution' 
        
        return operation_route[0],operation_route[1]


    def query_operator_route(self):   

        """routes files and string queries"""

        if  self._query_operator == 'FILE_QUERY':
                self._query = open(self._query,'r').read()
                return True, 'defined Query Operator'
        elif self._query_operator == 'STRING_QUERY':
                return True, 'defined Query Operator'
        else:
            return False, 'Undefined Query Operator'
