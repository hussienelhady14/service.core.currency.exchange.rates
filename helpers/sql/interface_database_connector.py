from abc import ABC, abstractmethod

class interface_database_connector(ABC):

    @abstractmethod
    def connect(self):
        pass

    @abstractmethod
    def disconnect(self):
        pass
