import pandas as pd
import yaml
from helpers.headers.interface_headers import interface_headers


class headers(interface_headers):

    def __init__(self,token):

        self._token = token
    
    @property
    def token(self):
        return self._token
    @token.setter
    def token(self, token):
        self._token = token

    def get_headers(self):
        
        return {
                        'Accepts': 'application/json',
                        'Content-Type': 'application/json',
                        'apikey': self._token
                }
