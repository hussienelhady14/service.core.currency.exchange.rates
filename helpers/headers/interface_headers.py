from abc import ABC, abstractmethod

class interface_headers(ABC):

    @abstractmethod
    def get_headers(self):
        pass
