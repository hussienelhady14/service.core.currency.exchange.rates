# service.core.currency.exchange.rates



## Getting started

This is a microservice repository written in "Python" using "Flask"

To use this repository follow the below steps

## Cloning repository

```
cd "Your preferred path"
git clone https://gitlab.com/hussienelhady14/service.core.currency.exchange.rates.git
```

## Installation

Docker has to be installed on your machine
```
docker-compose build
docker-compose up -d
```

To initialize the app run the following commands
```
docker exec -it exchange-service /bin/bash
```

Once you enter the container run the following commands
```
cd usr/src/app
pip install -r requirements.txt
python app.py
```

## Integrate with your tools

- Run sql statements in /config/db.sql (sql statements assume that the database used is a mysql db)
- Replace values (TOKEN, HOST, USERNAME, PASSWORD, SCHEMA) in /config/appconfig.yaml
- Replace values (SCHEMA) in /helpers/properties.py

## Test

After running
```
python app.py
```
Visit http://localhost/ui to explore the api usage (This is a swagger UI)

Test the following path with these example values http://localhost/exchangerates/2022-06-03/basecurrency/egp

Check the database for insertions -- currency_value represents the value of 1 EGP

![](images/image.png)

***
