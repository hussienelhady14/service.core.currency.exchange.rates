#Python-3.9 Debian image
FROM python:3.9-bullseye

ARG oraclepath="/opt/oracle/*"

#Download some packages to install odbc packages
RUN apt-get update && apt-get install build-essential \
 && apt-get install -y manpages-dev \
 && apt-get install  -y unixodbc-dev \
 && apt-get install -y alien \
 && apt-get install -y libpq-dev 


RUN apt-get install -y curl gnupg2 wget lsb-release unixodbc unzip

RUN apt-get install -y libaio1 libaio-dev gcc g++ ca-certificates

#Download SQL Server packages
RUN curl -k https://packages.microsoft.com/keys/microsoft.asc | apt-key add - \
&& curl -k https://packages.microsoft.com/config/debian/10/prod.list \
> /etc/apt/sources.list.d/mssql-release.list


#Download MySQL packages
RUN wget -q -O - https://repo.mysql.com/RPM-GPG-KEY-mysql-2022 | apt-key add - \
&& wget -P /etc/ https://dev.mysql.com/get/mysql-apt-config_0.8.17-1_all.deb --no-check-certificate\
&& dpkg -i /etc/mysql-apt-config_0.8.17-1_all.deb \
&& rm /etc/mysql-apt-config_0.8.17-1_all.deb \
&& wget -P /etc/ https://repo.mysql.com/apt/debian/pool/mysql-8.0/m/mysql-community/mysql-community-client-plugins_8.0.25-1debian10_amd64.deb --no-check-certificate\
&& dpkg -i /etc/mysql-community-client-plugins_8.0.25-1debian10_amd64.deb \
&& rm /etc/mysql-community-client-plugins_8.0.25-1debian10_amd64.deb 

#Download Oracle packages
RUN wget --no-check-certificate -P  /etc/ https://download.oracle.com/otn_software/linux/instantclient/instantclient-basic-linuxx64.zip \
&& wget --no-check-certificate  -P   /etc/ https://download.oracle.com/otn_software/linux/instantclient/instantclient-odbc-linuxx64.zip \
&& mkdir /opt/oracle \
&& unzip -d /opt/oracle /etc/instantclient-basic-linuxx64.zip \
&& rm /etc/instantclient-basic-linuxx64.zip \
&& unzip -d /opt/oracle /etc/instantclient-odbc-linuxx64.zip \
&& rm /etc/instantclient-odbc-linuxx64.zip \
&& touch /etc/odbcinst.ini \
&& cd $oraclepath && ./odbc_update_ini.sh / \
&& echo $oraclepath > /etc/ld.so.conf.d/oracle-instantclient.conf \
&& ldconfig \
&& export Oracle_Driver=$(echo ""| find /opt/oracle -name 'instantclient*' -type d)

#Install ODBC connectors
RUN apt-get update  -o Acquire::https::packages.microsoft.com::Verify-Peer=false\
&& ACCEPT_EULA=Y apt-get  install -y msodbcsql17 -o Acquire::https::packages.microsoft.com::Verify-Peer=false\
&& ACCEPT_EULA=Y apt-get install -y mssql-tools  -o Acquire::https::packages.microsoft.com::Verify-Peer=false\
&& apt-get install -y mysql-community-client-plugins mysql-connector-odbc odbc-postgresql \
&& sed -i 's#psqlodbcw.so#/usr/lib/x86_64-linux-gnu/odbc/psqlodbcw.so#' /etc/odbcinst.ini \
&& echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc 

#Clean the install
RUN apt-get -y clean && apt-get autoremove

#Tail container
CMD [ "tail", "-f", "/dev/null" ]