import json
from flask import Response, request
from helpers.properties import properties
from services.exchange_service import exchange



def put(date, cur):    
    #Initialize exchange service and fire the logic
    exchangeobj = exchange(date, cur)
    resp = exchangeobj.fire()

    #Assess response and return accordingly
    if(not resp[0]):
        message = resp[2]
        ret = Response(json.dumps(message), mimetype = properties._MIME_TYPE, status = int(resp[1]))
        return ret

    ret = Response(json.dumps(resp[2]), mimetype = properties._MIME_TYPE, status = int(resp[1]))
    return ret