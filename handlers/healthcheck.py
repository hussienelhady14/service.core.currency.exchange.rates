import json
from flask import Response, request
from helpers.properties import properties


def search():
    #Service health check
    message = {"message": 'healthy'}
    ret = Response(json.dumps(message), mimetype=properties._MIME_TYPE, status=200)
    return ret